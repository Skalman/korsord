## 2020-08-17

- Minor technical improvements

## 2020-06-14

- Create mode to show crossword without solution
- Support printing
- Keep track of where a crossword was forked from
- Minor refactor

## 2020-06-13

- Improved focus styling
- Minor refactor

## 2020-06-10

- Change the font
- Use a lighter background color for hints
- Center hint text when not editing

## 2020-06-09

- Technical improvements

## 2020-06-08

- Add footer
- Technical improvements

## 2020-06-07

- Support undo/redo
- Styling fix
- Add change log
- Add minimal introduction to the readme

## 2020-06-06

- Set up automatic deployment (CI/CD)
- Use license MPL-2.0
- Technical improvements and refactoring

## 2020-06-03

- Technical improvements and refactoring

## 2020-05-30

- Breaking change: Make the data format slightly more future-proof
- Add arrow decorations
- New split square mode: hint and letter
- Minor technical and styling improvements

## 2020-05-29

- Add toolbar with buttons (black and split cell)
- Minor technical improvements

## 2020-05-28

- Support black squares
- Square focus improvements

## 2020-05-27

- Technical improvements and refactoring

## 2020-05-26

- Autosave after 5 seconds
- Prevent edit conflicts
- Allow saving a copy
- Minor styling and technical improvements

## 2020-05-25

- Store crosswords on the server using PHP
- Technical improvements

## 2020-05-24

- Add minimal toolbar section
- Add favicon
- Styling, refactoring

## 2020-05-23

Initial release of Korsord, a web app for creating crossword puzzles.

Supported features:

- Create a crossword with letters and hints
- Hint squares can be split
