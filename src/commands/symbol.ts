import { areSymbolsCompatible, SymbolKey } from "../components/SvgSymbol";
import type Action from "../state/Action";
import type { MatrixCell } from "../state/State";
import arrayToggleValue from "../util/arrayToggleValue";
import { isBlack } from "./black";

export function hasSymbol(cell: MatrixCell, symbolKey: SymbolKey): boolean {
  return (
    cell.decorations?.find(
      (x) => x.type === "symbol" && x.key === symbolKey
    ) !== undefined
  );
}

export function canToggleSymbol(
  cell: MatrixCell,
  symbolKey: SymbolKey
): boolean {
  if (hasSymbol(cell, symbolKey)) {
    return true;
  }

  if (isBlack(cell)) {
    return false;
  }

  const hasIncompatible = cell.decorations?.some(
    (x) => x.type === "symbol" && !areSymbolsCompatible(symbolKey, x.key)
  );

  return !hasIncompatible;
}

export function getToggleSymbol(
  cell: MatrixCell,
  symbolKey: SymbolKey
): Action {
  if (!canToggleSymbol(cell, symbolKey)) {
    return { type: "noop" };
  }

  return {
    type: "setCellDecorations",
    cellId: cell.cellId,
    decorations: arrayToggleValue(
      cell.decorations ?? [],
      { type: "symbol", key: symbolKey },
      (x) => x.type === "symbol" && x.key === symbolKey
    ),
  };
}
