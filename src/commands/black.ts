import type Action from "../state/Action";
import type { MatrixCell } from "../state/State";
import arrayToggleValue from "../util/arrayToggleValue";

export function isBlack(cell: MatrixCell): boolean {
  return cell.decorations?.find((x) => x.type === "black") !== undefined;
}

function hasAnySymbol(cell: MatrixCell) {
  return cell.decorations?.some((x) => x.type === "symbol") === true;
}

export function canToggleBlack(cell: MatrixCell): boolean {
  return isBlack(cell) || (cell.type === "empty" && !hasAnySymbol(cell));
}

export function getToggleBlack(cell: MatrixCell): Action {
  if (!canToggleBlack(cell)) {
    return { type: "noop" };
  }

  return {
    type: "setCellDecorations",
    cellId: cell.cellId,
    decorations: arrayToggleValue(
      cell.decorations ?? [],
      { type: "black" },
      (x) => x.type === "black"
    ),
  };
}
