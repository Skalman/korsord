import type Action from "../state/Action";
import type { MatrixCell } from "../state/State";
import { isBlack } from "./black";

export function isVerticalSplit(cell: MatrixCell): boolean {
  return (
    cell.type === "split" &&
    cell.splitMode === "hintLetter" &&
    cell.splitHintOrientation === "vertical"
  );
}

export function canToggleVerticalSplit(cell: MatrixCell): boolean {
  return (
    !isBlack(cell) && (cell.type !== "split" || cell.splitMode === "hintLetter")
  );
}

export function getToggleVerticalSplit(cell: MatrixCell): Action {
  if (!canToggleVerticalSplit(cell)) {
    return { type: "noop" };
  }

  if (cell.type === "split") {
    return {
      type: "setCellContent",
      cellId: cell.cellId,
      content: cell.content,
      splitHintOrientation:
        cell.splitHintOrientation === "vertical" ? "horizontal" : "vertical",
    };
  } else {
    return {
      type: "setCellContent",
      cellId: cell.cellId,
      content: [cell.content ?? "", ""],
      splitHintOrientation: "vertical",
    };
  }
}
