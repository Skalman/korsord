import type Action from "../state/Action";
import type { MatrixCell } from "../state/State";
import { isBlack } from "./black";

export function isSplit(cell: MatrixCell): boolean {
  return cell.type === "split";
}

export function canToggleSplit(cell: MatrixCell): boolean {
  return !isBlack(cell);
}

export function getToggleSplit(cell: MatrixCell): Action {
  if (!canToggleSplit(cell)) {
    return { type: "noop" };
  }

  return {
    type: "setCellContent",
    cellId: cell.cellId,
    content:
      cell.type === "split" ? cell.content.join("") : [cell.content ?? "", ""],
  };
}
