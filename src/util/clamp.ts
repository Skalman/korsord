function clamp(lower: number, value: number, upper: number): number {
  return value < lower ? lower : upper < value ? upper : value;
}

export default clamp;
