export default function arrayUnique<T>(array: readonly T[]): T[] {
  return Array.from(new Set(array));
}
