/**
 * Checks that the parameters are arrays and that their contents are equal,
 * using `===`.
 */
export default function arraysEqual(a: unknown, b: unknown): boolean {
  return (
    Array.isArray(a) &&
    Array.isArray(b) &&
    a.length === b.length &&
    a.every((x, i) => x === b[i])
  );
}
