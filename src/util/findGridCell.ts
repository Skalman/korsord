import type { Matrix, MatrixCell } from "../state/State";

function findGridCellById(
  grid: Matrix,
  cellId: number
): { rowIndex: number; colIndex: number; cell?: MatrixCell } {
  for (let rowIndex = 0; rowIndex < grid.rows.length; rowIndex++) {
    const row = grid.rows[rowIndex];
    for (let colIndex = 0; colIndex < row.cells.length; colIndex++) {
      const cell = row.cells[colIndex];
      if (cell.cellId === cellId) {
        return { rowIndex, colIndex, cell };
      }
    }
  }

  return { rowIndex: -1, colIndex: -1 };
}

export default findGridCellById;
