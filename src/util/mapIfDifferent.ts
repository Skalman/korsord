function mapIfDifferent<T>(
  arr: readonly T[],
  callback: (item: T, index: number) => T
): readonly T[] {
  for (let i = 0; i < arr.length; i++) {
    const item = callback(arr[i], i);
    if (item !== arr[i]) {
      return [
        ...arr.slice(0, i),
        item,
        ...arr.slice(i + 1).map((item, j) => callback(item, i + 1 + j)),
      ];
    }
  }

  return arr;
}

export default mapIfDifferent;
