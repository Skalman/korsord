/**
 * If the array contains values matching the `predicate`, return a copy without
 * those values. Otherwise, return a copy with the `value` appended.
 */
function arrayToggleValue<T>(
  array: readonly T[],
  value: T,
  predicate: (item: T) => boolean
): readonly T[] {
  const result = array.filter((x) => !predicate(x));
  if (result.length === array.length) {
    result.push(value);
  }

  return result;
}

export default arrayToggleValue;
