function findLastIndex<T>(
  array: readonly T[],
  predicate: (item: T, index: number) => unknown
): number {
  for (let i = array.length - 1; i >= 0; i--) {
    if (predicate(array[i], i)) {
      return i;
    }
  }
  return -1;
}

export default findLastIndex;
