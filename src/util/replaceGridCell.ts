import type { Matrix, MatrixCell } from "../state/State";
import findGridCellById from "./findGridCell";

function replaceGridCellById(
  grid: Matrix,
  cellId: number,
  replacer: (oldCell: MatrixCell) => MatrixCell
): Matrix {
  const { rowIndex, colIndex, cell } = findGridCellById(grid, cellId);

  if (!cell) {
    return grid;
  }

  const replacement = replacer(cell);

  if (replacement === cell) {
    return grid;
  }

  return {
    ...grid,
    rows: grid.rows.map((row, i) => {
      if (i !== rowIndex) {
        return row;
      }

      return {
        ...row,
        cells: row.cells.map((cell, i) => {
          if (i !== colIndex) {
            return cell;
          }

          return replacement;
        }),
      };
    }),
  };
}

export default replaceGridCellById;
