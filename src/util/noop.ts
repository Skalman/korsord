function noop(): void {
  // Just an empty function
}

export default noop;
