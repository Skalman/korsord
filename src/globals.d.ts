declare namespace NodeJS {
  interface ProcessEnv {
    readonly REACT_APP_API_URL: string;
    readonly REACT_APP_BUILD_TIME?: string;
  }
}

// https://github.com/microsoft/TypeScript/issues/17002
interface ArrayConstructor {
  isArray(arg: unknown[]): arg is unknown[];
  isArray(arg: unknown): arg is readonly unknown[];
}
