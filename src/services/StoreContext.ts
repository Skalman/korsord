import React from "react";

import type { Matrix } from "../state/State";
import Store from "./Store";

const StoreContext = React.createContext(new Store<Matrix>("dummy://store"));

export default StoreContext;
