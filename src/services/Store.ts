interface GetResponse<T> {
  value: T;
  version: number;
}

interface CreateRequestBody<T> {
  value: T;
}

interface CreateResponse {
  id: string;
  password: string;
  version: number;
  url: string;
}

interface UpdateRequestBody<T> {
  value: T;
  password: string;
  version: number;
}

interface UpdateResponse {
  version: number;
  message: string;
}

class Store<T> {
  constructor(private readonly apiUrl: string) {}

  get(id: string): Promise<GetResponse<T>> {
    return this.getJson(this.getUrl(id));
  }

  create(value: T): Promise<CreateResponse> {
    return this.getJson(this.apiUrl, this.getFetchOptions("POST", { value }));
  }

  update(id: string, options: UpdateRequestBody<T>): Promise<UpdateResponse> {
    return this.getJson(this.getUrl(id), this.getFetchOptions("PUT", options));
  }

  private getUrl(id: string) {
    return `${this.apiUrl}?id=${encodeURIComponent(id)}`;
  }

  private getFetchOptions(
    method: "POST" | "PUT",
    body: CreateRequestBody<T> | UpdateRequestBody<T>
  ) {
    return {
      method,
      body: JSON.stringify(body),
      headers: { "Content-Type": "application/json" },
    };
  }

  private getErrorMessage(errorObject: { message?: string }) {
    return errorObject.message ?? JSON.stringify(errorObject);
  }

  private async getJson<TReturn>(url: string, fetchOptions?: RequestInit) {
    const response = await fetch(url, fetchOptions);

    let result: TReturn | { message?: string };
    try {
      result = await response.json();
    } catch (e) {
      console.error("Failed to parse JSON");
      throw e;
    }

    if (!response.ok) {
      throw new Error(this.getErrorMessage(result));
    }

    return result as TReturn;
  }
}

export default Store;
