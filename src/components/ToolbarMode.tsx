import classNames from "classnames";
import React from "react";

import type { Mode } from "../state/State";
import type StateContext from "../state/StateContext";

function ToolbarMode({
  dispatch,
  mode,
}: {
  dispatch: StateContext["dispatch"];
  mode: Mode;
}): JSX.Element {
  return (
    <p>
      <span className="mr-3">
        <button
          className={classNames({ active: mode === "edit" })}
          onClick={() => dispatch({ type: "changeMode", mode: "edit" })}
        >
          Redigera
        </button>
        <button
          className={classNames({ active: mode === "showWithoutSolution" })}
          onClick={() =>
            dispatch({ type: "changeMode", mode: "showWithoutSolution" })
          }
        >
          Visa
        </button>
      </span>
      <button onClick={() => window.print()}>Skriv ut</button>
    </p>
  );
}

export default ToolbarMode;
