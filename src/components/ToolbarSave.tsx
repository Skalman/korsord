import React, { memo, useEffect, useState } from "react";

import type Store from "../services/Store";
import type { Matrix } from "../state/State";
import type StateContext from "../state/StateContext";
import delay from "../util/delay";
import useDebounce from "../util/useDebounce";

interface Props {
  dispatch: StateContext["dispatch"];
  grid: Matrix;
  store: Store<Matrix>;
}

type SaveState =
  | "uninitialized"
  | "initialized"
  | "loading"
  | "loaded"
  | "waitForAutosave"
  | "saving";

const saveStateDisplayText: Record<SaveState, string> = {
  uninitialized: "Laddar...",
  initialized: "Inte sparad",
  loading: "Laddar...",
  loaded: "Laddad",
  waitForAutosave: "Sparad",
  saving: "Sparar...",
};

interface CrosswordMetadata {
  id: string;
  updateInfo?: {
    password: string;
    version: number;
  };
}

function ToolbarSave(props: Props) {
  const [metadata, setMetadata] = useState<CrosswordMetadata>();
  const [lastSaved, setLastSaved] = useState<
    | { grid: Matrix; autosaveDate?: Date }
    | { grid?: undefined; autosaveDate?: undefined }
  >({});
  const [state, setState] = useState<SaveState>("uninitialized");
  const [currentError, setCurrentError] = useState<string>();
  // Used to check if we should autosave.
  const [debouncedGrid, resetDebouncedGrid] = useDebounce(props.grid, 5_000);

  useEffect(() => {
    switch (state) {
      case "uninitialized":
        handleUninitialized();
        break;

      case "waitForAutosave":
        handleWaitForAutosave();
        break;
    }
  });

  function didSyncGrid(grid: Matrix, mode?: "autosave") {
    setLastSaved({
      grid,
      autosaveDate: mode === "autosave" ? new Date() : undefined,
    });
    resetDebouncedGrid(grid);
  }

  async function handleUninitialized() {
    const search = new URLSearchParams(window.location.search);
    const id = search.get("id");
    if (!id) {
      setState("initialized");
      return;
    }

    // Prevent editing while loading.
    props.dispatch({
      type: "replaceGrid",
      grid: {
        type: "korsord",
        version: 1,
        rows: [],
      },
    });

    setState("loading");
    let version: number;
    let grid: Matrix;
    try {
      const response = await props.store.get(id);
      version = response.version;
      grid = response.value;
    } catch (e) {
      // Back off, then reset the state, so that we'll try again later.
      await delay(5000);
      setState("uninitialized");
      return setCurrentError(formatError(e));
    }

    setCurrentError(undefined);

    props.dispatch({
      type: "replaceGrid",
      grid,
    });
    props.dispatch({
      type: "focusCellWithSelection",
      cellId: (grid.rows[2]?.cells[2] ?? grid.rows[0].cells[0]).cellId,
      selection: { part: 1, cursor: 0 },
    });
    // Assume that the change has propagated after this delay.
    await delay(0);

    didSyncGrid(grid);

    const password = search.get("password");
    if (password !== null) {
      setMetadata({
        id,
        updateInfo: { version, password },
      });
      setState("waitForAutosave");
    } else {
      setMetadata({ id });
      setState("loaded");
    }
  }

  async function handleWaitForAutosave() {
    if (
      lastSaved.grid === debouncedGrid ||
      metadata?.updateInfo === undefined
    ) {
      return;
    }

    // Autosave immediately.
    setState("saving");
    try {
      const response = await props.store.update(metadata.id, {
        version: metadata.updateInfo.version,
        password: metadata.updateInfo.password,
        value: debouncedGrid,
      });
      setMetadata({
        ...metadata,
        updateInfo: {
          ...metadata.updateInfo,
          version: response.version,
        },
      });
    } catch (e) {
      setCurrentError(formatError(e));
      // Back off, then reset the state, so that we'll try again later.
      await delay(5000);
      setState("waitForAutosave");
      return;
    }
    setCurrentError(undefined);
    didSyncGrid(debouncedGrid, "autosave");
    setState("waitForAutosave");
  }

  async function handleCreateClick() {
    setState("saving");

    let gridToSave: Matrix;

    if (metadata) {
      gridToSave = {
        ...props.grid,
        forkedFrom: window.location.href,
      };
      props.dispatch({ type: "replaceGrid", grid: gridToSave });
    } else {
      gridToSave = props.grid;
    }

    let newMetadata: Required<CrosswordMetadata>;
    try {
      const { id, password, version } = await props.store.create(gridToSave);
      newMetadata = {
        id,
        updateInfo: { password, version },
      };
    } catch (e) {
      // Reset the state.
      setCurrentError(formatError(e));
      return setState("initialized");
    }
    setCurrentError(undefined);

    didSyncGrid(gridToSave);
    setMetadata(newMetadata);

    const search = new URLSearchParams(window.location.search);
    search.set("id", newMetadata.id);
    search.set("password", newMetadata.updateInfo.password);
    window.history.replaceState(
      null,
      document.title,
      window.location.pathname + "?" + search.toString()
    );
    setState("waitForAutosave");
  }

  return (
    <>
      {currentError && <div className="alert alert-error">{currentError}</div>}
      <p>
        <SaveButton handleSaveClick={handleCreateClick} metadata={metadata} />
      </p>
      <p className="text-muted">
        <SaveState autosaveDate={lastSaved.autosaveDate} state={state} />
      </p>
    </>
  );
}

function SaveButton({
  handleSaveClick,
  metadata,
}: {
  handleSaveClick: () => void;
  metadata?: CrosswordMetadata;
}) {
  if (!metadata) {
    return <button onClick={handleSaveClick}>Spara nytt korsord</button>;
  } else if (!metadata.updateInfo) {
    return <button onClick={handleSaveClick}>Spara en kopia</button>;
  } else {
    return null;
  }
}

function SaveState({
  autosaveDate,
  state,
}: {
  autosaveDate?: Date;
  state: SaveState;
}) {
  if (autosaveDate && state !== "saving") {
    return (
      <>
        Autosparad{" "}
        <time
          dateTime={autosaveDate.toISOString()}
          title={autosaveDate.toISOString().replace(/\.\d+/, "")}
        >
          {autosaveDate.toTimeString().substr(0, 5)}
        </time>
      </>
    );
  }

  return <>{saveStateDisplayText[state]}</>;
}

function formatError(error: { message?: string }) {
  return error.message ?? error.toString();
}

export default memo(ToolbarSave);
