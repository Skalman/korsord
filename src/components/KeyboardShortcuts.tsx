import Mousetrap from "mousetrap";
import { Component, Dispatch } from "react";

import type Action from "../state/Action";

interface Props {
  dispatch: Dispatch<Action>;
}

interface State {
  mousetrap: MousetrapInstance;
}

class KeyboardShortcuts extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { mousetrap: new Mousetrap() };
  }

  componentDidMount(): void {
    const { dispatch } = this.props;
    const { mousetrap } = this.state;

    mousetrap.stopCallback = () => false;
    mousetrap.bind(["ctrl+b", "meta+b"], (e) => {
      e.preventDefault();
      dispatch({ type: "cellCommand", command: "toggleBlack" });
    });

    mousetrap.bind(["ctrl+z", "meta+z"], (e) => {
      e.preventDefault();
      dispatch({ type: "historyGo", jump: -1 });
    });

    mousetrap.bind(
      ["ctrl+shift+z", "ctrl+y", "meta+shift+z", "meta+y"],
      (e) => {
        e.preventDefault();
        dispatch({ type: "historyGo", jump: 1 });
      }
    );
  }

  componentWillUnmount(): void {
    this.state.mousetrap.reset();
  }

  render(): null {
    return null;
  }
}

export default KeyboardShortcuts;
