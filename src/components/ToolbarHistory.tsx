import "./ToolbarHistory.css";

import React from "react";

import type { History } from "../state/State";
import type StateContext from "../state/StateContext";

function ToolbarHistory({
  dispatch,
  history,
}: {
  dispatch: StateContext["dispatch"];
  history: History;
}): JSX.Element {
  return (
    <div className="ToolbarHistory">
      <button
        disabled={history.past.length === 0}
        onClick={() => dispatch({ type: "historyGo", jump: -1 })}
      >
        &#x21b6;
      </button>
      <button
        disabled={history.future.length === 0}
        onClick={() => dispatch({ type: "historyGo", jump: 1 })}
      >
        &#x21b7;
      </button>
    </div>
  );
}

export default ToolbarHistory;
