import "./ToolbarCell.css";

import React, { memo, useState } from "react";

import { canToggleBlack, isBlack } from "../commands/black";
import { canToggleSplit, isSplit } from "../commands/split";
import { canToggleSymbol, hasSymbol } from "../commands/symbol";
import {
  canToggleVerticalSplit,
  isVerticalSplit,
} from "../commands/verticalSplit";
import type { MatrixCell } from "../state/State";
import type StateContext from "../state/StateContext";
import arrayUnique from "../util/arrayUnique";
import findGridCellById from "../util/findGridCell";
import GridCell from "./GridCell";
import { symbolCategories, SymbolKey } from "./SvgSymbol";

const metaKey = /(Mac|iPhone|iPod|iPad)/i.test(navigator.platform)
  ? "\u2318" // Looped square.
  : "Ctrl";

interface Props {
  dispatch: StateContext["dispatch"];
  cell: MatrixCell;
}

function ToolbarCell({ dispatch, state }: StateContext): JSX.Element | null {
  const cell = state.focusedCell
    ? findGridCellById(state.grid, state.focusedCell.cellId).cell
    : undefined;

  if (!cell) {
    return null;
  }

  return (
    <>
      <h3>Kommandon</h3>
      <ToolbarCellTableMemo dispatch={dispatch} cell={cell} />
    </>
  );
}

const ToolbarCellTableMemo = memo(ToolbarCellTable);

function ToolbarCellTable({ dispatch, cell }: Props): JSX.Element {
  const [symbolCategory, setSymbolCategory] = useState(symbolCategories.common);

  return (
    <div className="ToolbarCell-commands">
      <div className="ToolbarCell-row">
        <div>
          <CellButton
            active={isBlack(cell)}
            onClick={() =>
              dispatch({ type: "cellCommand", command: "toggleBlack" })
            }
            disabled={!canToggleBlack(cell)}
            cell={{
              type: "empty",
              cellId: -1,
              content: undefined,
              decorations: [{ type: "black" }],
            }}
          />
          <div className={getCommandClass(canToggleBlack(cell))}>
            <kbd>{metaKey}</kbd> + <kbd>B</kbd>
          </div>
        </div>
        <div>
          <CellButton
            active={isSplit(cell)}
            onClick={() =>
              dispatch({ type: "cellCommand", command: "toggleSplit" })
            }
            disabled={!canToggleSplit(cell)}
            cell={{
              type: "split",
              splitMode: "hint",
              cellId: -1,
              content: ["delad", "ruta"],
            }}
          />
          <div className={getCommandClass(canToggleSplit(cell))}>
            <kbd>-</kbd>
            <kbd>-</kbd>
            <kbd>-</kbd>
          </div>
        </div>
        <div>
          <CellButton
            active={isVerticalSplit(cell)}
            onClick={() =>
              dispatch({ type: "cellCommand", command: "toggleVerticalSplit" })
            }
            disabled={!canToggleVerticalSplit(cell)}
            cell={{
              type: "split",
              splitMode: "hintLetter",
              splitHintOrientation: "vertical",
              cellId: -1,
              content: ["vertikal", ""],
            }}
          />
          <div className={getCommandClass(canToggleVerticalSplit(cell))}>
            <kbd>|</kbd>
          </div>
        </div>
      </div>
      <div className="ToolbarCell-categories">
        <CellButton
          rounded={true}
          active={symbolCategory === symbolCategories.common}
          onClick={() => setSymbolCategory(symbolCategories.common)}
          cell={{
            type: "empty",
            cellId: -1,
            content: undefined,
            decorations: [
              { type: "symbol", key: "arrowNE" },
              { type: "symbol", key: "arrowWS" },
            ],
          }}
        />
        <CellButton
          rounded={true}
          active={symbolCategory === symbolCategories.toEast.all}
          onClick={() => setSymbolCategory(symbolCategories.toEast.all)}
          cell={{
            type: "empty",
            cellId: -1,
            content: undefined,
            decorations: [{ type: "symbol", key: "largeArrowWE" }],
          }}
        />
        <CellButton
          rounded={true}
          active={symbolCategory === symbolCategories.toSouth.all}
          onClick={() => setSymbolCategory(symbolCategories.toSouth.all)}
          cell={{
            type: "empty",
            cellId: -1,
            content: undefined,
            decorations: [{ type: "symbol", key: "largeArrowNS" }],
          }}
        />
      </div>
      <div>
        {arrayUnique(
          symbolCategory.concat(
            cell.decorations
              ?.filter(
                (x): x is { type: "symbol"; key: SymbolKey } =>
                  x.type === "symbol"
              )
              .map((x) => x.key) ?? []
          )
        ).map((x) => (
          <SymbolButton key={x} dispatch={dispatch} cell={cell} symbolKey={x} />
        ))}
      </div>
    </div>
  );
}

function SymbolButton({
  dispatch,
  cell,
  symbolKey,
}: Props & { symbolKey: SymbolKey }) {
  return (
    <CellButton
      active={hasSymbol(cell, symbolKey)}
      onClick={() =>
        dispatch({ type: "cellCommand", command: "toggleSymbol", symbolKey })
      }
      disabled={!canToggleSymbol(cell, symbolKey)}
      cell={{
        type: "empty",
        cellId: -1,
        content: undefined,
        decorations: [{ type: "symbol", key: symbolKey }],
      }}
    />
  );
}

function CellButton({
  active,
  disabled,
  rounded,
  onClick,
  cell,
}: {
  active: boolean;
  disabled?: boolean;
  rounded?: boolean;
  onClick: () => void;
  cell: MatrixCell;
}): JSX.Element {
  return (
    <button
      className={active ? "active" : undefined}
      onClick={onClick}
      disabled={disabled}
    >
      <GridCell
        inertMode={active ? "focus" : "normal"}
        cell={cell}
        rounded={rounded}
      />
    </button>
  );
}

function getCommandClass(enabled: boolean) {
  return enabled ? undefined : "ToolbarCell-disabled";
}

export default ToolbarCell;
