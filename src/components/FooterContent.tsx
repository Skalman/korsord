import React, { memo } from "react";

import { version } from "../../package.json";

function FooterContent(): JSX.Element {
  const buildTime =
    process.env.REACT_APP_BUILD_TIME &&
    new Date(process.env.REACT_APP_BUILD_TIME).toISOString().substr(0, 10);

  const versionString =
    `Version ${version}` + (buildTime ? ` (${buildTime})` : "");

  return (
    <ul className="list-inline">
      <li>
        Korsord av <a href="https://danwolff.se">Dan Wolff</a>
      </li>
      <li>
        <a href="https://gitlab.com/Skalman/korsord">Källkod</a>
      </li>
      <li>{versionString}</li>
      <li>
        <a href={`${process.env.PUBLIC_URL}/CHANGELOG.md`}>Ändringar</a>
      </li>
    </ul>
  );
}

export default memo(FooterContent);
