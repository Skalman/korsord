import "./GridCell.css";

import classNames from "classnames";
import React, { memo, useEffect, useRef, useState } from "react";

import type { FocusedCell, MatrixCell, Mode } from "../state/State";
import StateContext from "../state/StateContext";
import noop from "../util/noop";
import SvgSymbol from "./SvgSymbol";

function GridCell({
  cell,
  inertMode,
  rounded,
}: {
  cell: MatrixCell;
  inertMode?: "normal" | "focus";
  rounded?: boolean;
  fakeFocus?: boolean;
}): JSX.Element {
  if (inertMode) {
    return (
      <GridCellInnerMemo
        cell={cell}
        dispatch={noop}
        inertMode={inertMode}
        rounded={rounded}
        mode={"edit"}
      />
    );
  }

  return (
    <StateContext.Consumer>
      {({ dispatch, state }) => {
        const focusSelection =
          state.focusedCell?.cellId === cell.cellId
            ? state.focusedCell.selection
            : undefined;

        return (
          <GridCellInnerMemo
            cell={cell}
            dispatch={dispatch}
            focusSelection={focusSelection}
            rounded={rounded}
            mode={state.mode}
          />
        );
      }}
    </StateContext.Consumer>
  );
}

const GridCellInnerMemo = memo(GridCellInner);

function GridCellInner({
  cell,
  dispatch,
  focusSelection,
  inertMode,
  rounded,
  mode,
}: {
  cell: MatrixCell;
  dispatch: StateContext["dispatch"];
  focusSelection?: FocusedCell["selection"];
  inertMode?: "normal" | "focus";
  rounded?: boolean;
  mode: Mode;
}): JSX.Element {
  const [forceFocus, setForceFocus] = useState(false);
  const textarea1 = useRef<HTMLTextAreaElement | null>(null);
  const textarea2 = useRef<HTMLTextAreaElement | null>(null);
  const isBlack = Boolean(cell.decorations?.find((x) => x.type === "black"));

  useEffect(() => {
    if (!focusSelection) {
      return;
    }

    focusTextarea(
      focusSelection.part === 2 && cell.type === "split"
        ? textarea2
        : textarea1,
      focusSelection.cursor,
      forceFocus
    );

    if (forceFocus) {
      setForceFocus(false);
    }
  }, [focusSelection, cell, forceFocus]);

  function handleChange(e: React.ChangeEvent<HTMLTextAreaElement>) {
    const newContent = e.target.value;
    let content: string | [string, string];
    if (cell.type === "split") {
      if (isFirstTextarea(e)) {
        content = [newContent, cell.content[1]];
      } else {
        content = [cell.content[0], newContent];
      }
    } else {
      content = newContent;
    }

    dispatch({
      type: "setCellContent",
      cellId: cell.cellId,
      content,
    });
  }

  function handleKeyDown(e: React.KeyboardEvent<HTMLTextAreaElement>) {
    const textarea = e.currentTarget;
    const info: KeyboardEventInfo = {
      preventDefault: e.preventDefault.bind(e),
      key: e.key,
      ctrlMetaAlt: e.ctrlKey || e.metaKey || e.altKey,
      shiftKey: e.shiftKey,
      cell,
      isFirstTextarea: isFirstTextarea(e),
      selectionIsEmpty: textarea.selectionStart === textarea.selectionEnd,
      selectionIsAtStart: textarea.selectionStart === 0,
      selectionIsAtEnd: textarea.selectionEnd === textarea.value.length,
    };

    const focusDirection = getNextFocusDirection(info);
    switch (focusDirection) {
      case "first":
      case "second":
        return dispatch({
          type: "focusCellWithSelection",
          cellId: cell.cellId,
          selection:
            focusDirection === "first"
              ? { part: 1, cursor: 0 }
              : { part: 2, cursor: 1000 },
        });

      case "up":
      case "right":
      case "down":
      case "left":
        return dispatch({
          type: "focusCellMove",
          move: focusDirection,
        });
    }

    const merge = getMerge(info);
    if (merge) {
      dispatch({
        type: "setCellContent",
        cellId: cell.cellId,
        content: merge.join(""),
      });

      setForceFocus(true);
      return dispatch({
        type: "focusCellWithSelection",
        cellId: cell.cellId,
        selection: {
          part: 1,
          cursor: merge[0].length,
        },
      });
    }
  }

  function handleFocus(e: React.FocusEvent<HTMLTextAreaElement>) {
    e.currentTarget.scrollIntoView({
      behavior: "smooth",
      block: "center",
      inline: "center",
    });

    const part = isFirstTextarea(e) ? 1 : 2;
    if (focusSelection?.part !== part) {
      dispatch({
        type: "focusCellWithSelection",
        cellId: cell.cellId,
        selection: {
          part,
          cursor: 0,
        },
      });
    }
  }

  function isFirstTextarea(e: React.SyntheticEvent<HTMLTextAreaElement>) {
    return e.currentTarget === textarea1.current;
  }

  const [content1, content2] = getContentToDisplay(mode, cell);

  const isHint1 = cell.type === "hint" || cell.type === "split";
  const isHint2 = cell.type === "split" && cell.splitMode === "hint";

  const isReadOnly =
    isBlack ||
    mode === "showWithoutSolution" ||
    (mode === "solve" && cell.type === "empty");
  const isHintReadOnly = isReadOnly || mode === "solve";

  const isReadOnly1 = isReadOnly || (isHint1 && isHintReadOnly);
  const isReadOnly2 = isReadOnly || (isHint2 && isHintReadOnly);

  const textarea1Class = classNames("GridCell-textarea GridCell-textarea1", {
    "GridCell-focus": focusSelection?.part === 1 || inertMode === "focus",
    "GridCell-hint": isHint1,
  });

  const textarea2Class = classNames("GridCell-textarea GridCell-textarea2", {
    "GridCell-focus": focusSelection?.part === 2 || inertMode === "focus",
    "GridCell-hint": isHint2,
  });

  return (
    <div
      className={classNames(
        `GridCell GridCell-${cell.type}`,
        {
          "GridCell-has-focus":
            focusSelection !== undefined || inertMode === "focus",
          "GridCell-inert": inertMode !== undefined,
          "GridCell-rounded": rounded,
          "GridCell-is-margin": cell.isMargin,
        },
        cell.type === "split" && {
          "GridCell-split-equal": cell.splitMode === "hint",
          "GridCell-split-horizontal":
            cell.splitMode === "hintLetter" &&
            (cell.splitHintOrientation ?? "horizontal") === "horizontal",
          "GridCell-split-vertical":
            cell.splitMode === "hintLetter" &&
            cell.splitHintOrientation === "vertical",
        }
      )}
      title={cell.cellId.toString()}
    >
      <textarea
        ref={textarea1}
        value={content1}
        tabIndex={inertMode ? -1 : undefined}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        onFocus={handleFocus}
        readOnly={isReadOnly1}
        className={textarea1Class}
      />
      {content2 === undefined ? undefined : (
        <textarea
          ref={textarea2}
          value={content2}
          tabIndex={inertMode ? -1 : undefined}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          onFocus={handleFocus}
          readOnly={isReadOnly2}
          className={textarea2Class}
        />
      )}
      <div className="GridCell-overlay">
        <div className={textarea1Class}>{content1}</div>
        {content2 === undefined ? undefined : (
          <div className={textarea2Class}>{content2}</div>
        )}
      </div>
      <div className="GridCell-symbols">
        {isBlack && (
          // Use an <svg> instead of a background so that it'll print correctly.
          <div className="GridCell-decoration-black">
            <SvgSymbol symbol="black" />
          </div>
        )}
        {cell.decorations?.map((decoration) => {
          if (typeof decoration === "object" && decoration.type === "symbol") {
            return <SvgSymbol key={decoration.key} symbol={decoration.key} />;
          }
        })}
      </div>
    </div>
  );
}

const focusDirections = {
  first: { left: "left", up: "up", right: "second", down: "second" },
  second: { left: "first", up: "first", right: "right", down: "down" },
} as const;

interface KeyboardEventInfo {
  preventDefault(): void;
  key: string;
  ctrlMetaAlt: boolean;
  shiftKey: boolean;
  cell: MatrixCell;
  isFirstTextarea: boolean;
  selectionIsEmpty: boolean;
  selectionIsAtStart: boolean;
  selectionIsAtEnd: boolean;
}

function getNextFocusDirection(e: KeyboardEventInfo) {
  const move = getNextFocusDirectionInner(e);

  if (!move) {
    return;
  }

  e.preventDefault();

  if (e.cell.type !== "split") {
    return move;
  } else if (e.isFirstTextarea) {
    return focusDirections.first[move];
  } else {
    return focusDirections.second[move];
  }
}

function getNextFocusDirectionInner(e: KeyboardEventInfo) {
  if (e.ctrlMetaAlt) {
    return;
  }

  if (e.key === "Enter") {
    if (e.shiftKey) {
      return "up";
    } else {
      return "down";
    }
  }

  if (e.shiftKey) {
    return;
  }

  if (e.selectionIsAtStart) {
    if (e.key === "ArrowUp") {
      return "up";
    } else if (e.key === "ArrowLeft") {
      return "left";
    }
  }

  if (e.selectionIsAtEnd) {
    if (e.key === "ArrowDown") {
      return "down";
    } else if (e.key === "ArrowRight") {
      return "right";
    }
  }
}

function focusTextarea(
  { current }: React.MutableRefObject<HTMLTextAreaElement | null>,
  cursor: number,
  forceFocus: boolean
) {
  if (!current || (document.activeElement === current && !forceFocus)) {
    return;
  }

  current.focus();
  current.selectionStart = current.selectionEnd = cursor;
}

function getMerge(e: KeyboardEventInfo) {
  if (e.ctrlMetaAlt || e.cell.type !== "split" || !e.selectionIsEmpty) {
    return;
  }

  const mergeFromFirst =
    e.isFirstTextarea && e.key === "Delete" && e.selectionIsAtEnd;
  const mergeFromSecond =
    !e.isFirstTextarea && e.key === "Backspace" && e.selectionIsAtStart;

  const merge = mergeFromFirst || mergeFromSecond;
  if (!merge) {
    return;
  }

  e.preventDefault();

  return e.cell.content;
}

function getContentToDisplay(
  mode: Mode,
  cell: MatrixCell
): readonly [string, string?] {
  if (mode === "showWithoutSolution") {
    if (cell.type === "letter") {
      return [""];
    } else if (cell.type === "split" && cell.splitMode === "hintLetter") {
      return [cell.content[0], ""];
    }
  }

  return Array.isArray(cell.content) ? cell.content : [cell.content ?? ""];
}

export default GridCell;
