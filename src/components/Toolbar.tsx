import React from "react";

import StoreContext from "../services/StoreContext";
import StateContext from "../state/StateContext";
import ToolbarCell from "./ToolbarCell";
import ToolbarHistory from "./ToolbarHistory";
import ToolbarMode from "./ToolbarMode";
import ToolbarSave from "./ToolbarSave";

function Toolbar(): JSX.Element {
  return (
    <>
      <StoreContext.Consumer>
        {(store) => (
          <StateContext.Consumer>
            {({ dispatch, state }) => {
              return (
                <>
                  <ToolbarSave
                    dispatch={dispatch}
                    grid={state.grid}
                    store={store}
                  />
                  <ToolbarMode dispatch={dispatch} mode={state.mode} />
                  {(state.mode === "edit" || state.mode === "solve") && (
                    <ToolbarHistory
                      dispatch={dispatch}
                      history={state.history}
                    />
                  )}
                  {state.mode === "edit" && (
                    <ToolbarCell dispatch={dispatch} state={state} />
                  )}
                </>
              );
            }}
          </StateContext.Consumer>
        )}
      </StoreContext.Consumer>
    </>
  );
}

export default Toolbar;
