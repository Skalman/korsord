import "./Grid.css";

import React from "react";

import type { Matrix } from "../state/State";
import GridCell from "./GridCell";

function Grid(props: { grid: Matrix }): JSX.Element {
  return (
    <div className="Grid">
      {props.grid.rows.map((row) => (
        <div className="Grid-row" key={row.rowId}>
          {row.cells.map((cell) => (
            <GridCell key={cell.cellId} cell={cell} />
          ))}
        </div>
      ))}
    </div>
  );
}

export default Grid;
