import "./SvgSymbol.css";

import React, { memo } from "react";

function generateArrows() {
  const arrowNwE = <path d="M4 0 L4 3 L5 4 L10 4 L8 2 M10 4 L8 6" />;
  const arrowNeE = <g transform="translate(18 0)">{arrowNwE}</g>;
  const arrowNE = <g transform="translate(0 24)">{arrowNwE}</g>;
  const arrowSwE = <g transform="scale(1 -1)">{arrowNwE}</g>;
  const arrowSeE = <g transform="scale(1 -1) translate(18 0)">{arrowNwE}</g>;

  const arrowNwS = <g transform="scale(-1 1) rotate(90)">{arrowNwE}</g>;
  const arrowSwS = (
    <g transform="scale(-1 1) rotate(90) translate(18 0)">{arrowNwE}</g>
  );
  const arrowWS = (
    <g transform="scale(-1 1) rotate(90) translate(0 24)">{arrowNwE}</g>
  );
  const arrowNeS = <g transform="scale(1 1) rotate(90)">{arrowNwE}</g>;
  const arrowSeS = (
    <g transform="scale(1 1) rotate(90) translate(18 0)">{arrowNwE}</g>
  );

  return {
    arrowNwE,
    arrowNeE,
    arrowNE,
    arrowSwE,
    arrowSeE,

    arrowNwS,
    arrowSwS,
    arrowWS,
    arrowNeS,
    arrowSeS,
  };
}

function generateArrowDiags() {
  const arrowDiagNwE = <path d="M0 0 L4 4 L10 4 L8 2 M10 4 L8 6" />;
  const arrowDiagSwE = <g transform="scale(1 -1)">{arrowDiagNwE}</g>;

  const arrowDiagNwS = <g transform="scale(-1 1) rotate(90)">{arrowDiagNwE}</g>;
  const arrowDiagNeS = <g transform="scale(1 1) rotate(90)">{arrowDiagNwE}</g>;

  return {
    arrowDiagNwE,
    arrowDiagSwE,

    arrowDiagNwS,
    arrowDiagNeS,
  };
}

function generateTriangles() {
  const triangleWE = <path d="M0 14 L2 16 L0 18" fill="currentColor" />;
  const triangleNS = <g transform="rotate(90)">{triangleWE}</g>;

  return { triangleWE, triangleNS };
}

function generateLargeTriangles() {
  const largeTriangleWE = <path d="M0 10 L4 16 L0 22" fill="currentColor" />;
  const largeTriangleNS = <g transform="rotate(90)">{largeTriangleWE}</g>;

  return { largeTriangleWE, largeTriangleNS };
}

function generateLargeArrows() {
  const largeArrowWE = (
    <path
      d="M0 14 L0 18 L25 18 L25 22 L31 16 L25 10 L25 14 L0 14"
      fill="currentColor"
    />
  );
  const largeArrowNS = <g transform="rotate(90)">{largeArrowWE}</g>;

  const largeArrowNE = (
    <path
      d="M14 0 L14 18 L25 18 L25 22 L31 16 L25 10 L25 14 L18 14 L18 0 L14 0"
      fill="currentColor"
    />
  );
  const largeArrowSE = <g transform="scale(1 -1)">{largeArrowNE}</g>;
  const largeArrowES = <g transform="rotate(90)">{largeArrowNE}</g>;
  const largeArrowWS = <g transform="scale(-1 1) rotate(90)">{largeArrowNE}</g>;

  return {
    largeArrowWE,
    largeArrowNS,

    largeArrowNE,
    largeArrowSE,
    largeArrowES,
    largeArrowWS,
  };
}

function generateLargeLines() {
  const largeLineWE = (
    <path d="M0 14 L0 18 L32 18 L32 14 L0 14" fill="currentColor" />
  );
  const largeLineNS = <g transform="rotate(90)">{largeLineWE}</g>;

  return { largeLineWE, largeLineNS };
}

function generateBlack() {
  const black = <rect width={32} height={32} fill="#000" />;

  return { black };
}

function generateDecorations() {
  return {
    ...generateArrows(),
    ...generateArrowDiags(),
    ...generateTriangles(),
    ...generateLargeTriangles(),
    ...generateLargeArrows(),
    ...generateLargeLines(),
    ...generateBlack(),
  };
}

const symbols = generateDecorations();

/**
 * The decoration keys use a naming scheme. E.g. `arrowDiagNeS` can be split
 * into parts:
 *
 * - `arrowDiag` - The first part indicates the symbol shape.
 * - `NeS` - The suffix indicates the direction. This arrow starts from the
 *    north-east corner and points southward.
 */
export type SymbolKey = keyof typeof symbols;

const all = Object.keys(symbols) as SymbolKey[];

export const symbolCategories = {
  common: ["arrowNE", "arrowNwE", "arrowWS", "arrowNwS"] as SymbolKey[],

  toEast: {
    all: all.filter((x) => /E$/.test(x)),
    fromNorth: all.filter((x) => /N[we]?E$/.test(x)),
    fromSouth: all.filter((x) => /S[we]?E$/.test(x)),
    fromWest: all.filter((x) => /WE$/.test(x)),
  },

  toSouth: {
    all: all.filter((x) => /S$/.test(x)),
    fromWest: all.filter((x) => /[Ww]S$/.test(x)),
    fromEast: all.filter((x) => /[Ee]S$/.test(x)),
    fromNorth: all.filter((x) => /NS$/.test(x)),
  },

  largeCellSymbols: all.filter((x) => /^large/.test(x) && !/Triangle/.test(x)),
  smallCellSymbols: all.filter((x) => /^arrow|[tT]riangle/.test(x)),

  black: "black" as SymbolKey,

  all,
};

export function areSymbolsCompatible(a: SymbolKey, b: SymbolKey): boolean {
  return (
    a === b ||
    symbolCategories.smallCellSymbols.includes(a) ||
    symbolCategories.smallCellSymbols.includes(b)
  );
}

function SvgSymbol({ symbol }: { symbol: SymbolKey }): JSX.Element {
  return (
    <svg className="SvgSymbol" viewBox="0 0 32 32" width="32" height="32">
      {symbols[symbol]}
    </svg>
  );
}

export default memo(SvgSymbol);
