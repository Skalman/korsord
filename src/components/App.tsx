import "./App.css";

import React, { useReducer } from "react";

import Store from "../services/Store";
import StoreContext from "../services/StoreContext";
import initialState from "../state/initialState";
import { historyEnhancedReducer } from "../state/reducer";
import type { Matrix } from "../state/State";
import StateContext from "../state/StateContext";
import FooterContent from "./FooterContent";
import Grid from "./Grid";
import KeyboardShortcuts from "./KeyboardShortcuts";
import Toolbar from "./Toolbar";

function App(): JSX.Element {
  const [state, dispatch] = useReducer(historyEnhancedReducer, initialState);

  return (
    <>
      <KeyboardShortcuts dispatch={dispatch} />
      <StateContext.Provider value={{ state, dispatch }}>
        <StoreContext.Provider
          value={new Store<Matrix>(process.env.REACT_APP_API_URL)}
        >
          <div className="App">
            <div className="App-toolbar d-print-none">
              <h1>Skapa korsord</h1>

              <p>Fyll i rutorna och skapa ett korsord.</p>

              <Toolbar />

              <footer className="text-muted">
                <FooterContent />
              </footer>
            </div>
            <div className="App-editor">
              <Grid grid={state.grid}></Grid>
            </div>
          </div>
        </StoreContext.Provider>
      </StateContext.Provider>
    </>
  );
}

export default App;
