import { ensureMarginReducer } from "./actions/ensureMargin";
import { focusCellWithSelectionReducer } from "./actions/focusCellWithSelection";
import type State from "./State";
import type { Matrix } from "./State";

// This is the initial state, which will be transformed a couple of times
// before we're done.
const initialGrid: Matrix = {
  type: "korsord",
  version: 1,
  rows: [],
};

let initialState: State = {
  mode: "edit",
  grid: initialGrid,

  history: {
    past: [],
    current: { grid: initialGrid },
    future: [],
  },
};

initialState = ensureMarginReducer(initialState, {
  type: "ensureMargin",
});

initialState = focusCellWithSelectionReducer(initialState, {
  type: "focusCellWithSelection",
  cellId: initialState.grid.rows[2].cells[2].cellId,
  selection: {
    part: 1,
    cursor: 0,
  },
});

export default initialState;
