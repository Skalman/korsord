import type State from "../State";

export interface FocusCellBlurAction {
  type: "focusCellBlur";
}

export function focusCellBlurReducer(
  state: State,
  _action: FocusCellBlurAction
): State {
  if (state.focusedCell === undefined) {
    return state;
  }

  return {
    ...state,
    focusedCell: undefined,
  };
}
