import type State from "../State";
import type { FocusedCell, History, HistoryItem } from "../State";

const maxHistoryLength = 50;

export interface HistoryUpdateAction {
  type: "historyUpdate";
  prevState: State;
}

export function historyUpdateReducer(
  state: State,
  { prevState }: HistoryUpdateAction
): State {
  const history = getUpdatedHistory(state, prevState);

  if (!history) {
    return state;
  }

  return {
    ...state,
    history,
  };
}

function getUpdatedHistory(
  state: State,
  prevState: State
): History | undefined {
  if (state === prevState) {
    return;
  }

  const historyChanged = state.history !== prevState.history;
  if (historyChanged) {
    return;
  }

  const isGridSame = state.grid === prevState.grid;
  if (isGridSame) {
    return;
  }

  const { past, current } = state.history;

  const isInitialChange = past.length === 0;
  if (isInitialChange) {
    return {
      past: [getItem(prevState)],
      current: getItem(state),
      future: [],
    };
  }

  const newCellChanged = !isSameFocus(state.focusedCell, current.focusedCell);

  return {
    past: newCellChanged ? addCurrentToPast(past, current) : past,
    current: getItem(state),
    future: [],
  };
}

function getItem(state: State): HistoryItem {
  return {
    grid: state.grid,
    focusedCell: state.focusedCell,
  };
}

function isSameFocus(a?: FocusedCell, b?: FocusedCell) {
  if (a === b) {
    return true;
  }

  return (
    a !== undefined &&
    b !== undefined &&
    a.cellId === b.cellId &&
    a.selection.part === b.selection.part
  );
}

function addCurrentToPast(past: readonly HistoryItem[], current: HistoryItem) {
  if (past.length >= maxHistoryLength) {
    const copy = past.slice(1 - maxHistoryLength);
    copy.push(current);
    return copy;
  } else {
    return [...past, current];
  }
}
