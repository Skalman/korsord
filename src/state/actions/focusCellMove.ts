import findGridCellById from "../../util/findGridCell";
import type { MatrixCell } from "../State";
import type State from "../State";

export interface FocusCellMoveAction {
  type: "focusCellMove";
  move: keyof typeof moves;
}

const moves = {
  up: {
    rowAmount: -1,
    colAmount: 0,
    side: "start",
  },
  down: {
    rowAmount: 1,
    colAmount: 0,
    side: "end",
  },
  left: {
    rowAmount: 0,
    colAmount: -1,
    side: "start",
  },
  right: {
    rowAmount: 0,
    colAmount: 1,
    side: "end",
  },
} as const;

export function focusCellMoveReducer(
  state: State,
  action: FocusCellMoveAction
): State {
  const cell = getCell(state, action);

  if (!cell) {
    return state;
  }

  const selection = getSelection(cell, action);

  return {
    ...state,
    focusedCell: {
      cellId: cell.cellId,
      selection,
    },
  };
}

function getCell(state: State, action: FocusCellMoveAction) {
  const cellId = state.focusedCell?.cellId;
  if (cellId === undefined) {
    return;
  }

  const { move } = action;
  let { rowIndex, colIndex } = findGridCellById(state.grid, cellId);

  if (rowIndex === -1 && colIndex === -1) {
    return;
  }

  const dir = moves[move];

  rowIndex += dir.rowAmount;
  colIndex += dir.colAmount;

  const rows = state.grid.rows;

  const isWithinBounds =
    0 <= rowIndex &&
    rowIndex < rows.length &&
    0 <= colIndex &&
    colIndex < rows[0].cells.length;

  if (!isWithinBounds) {
    return;
  }

  return rows[rowIndex].cells[colIndex];
}

function getSelection(
  cell: MatrixCell,
  action: FocusCellMoveAction
): { part: 1 | 2; cursor: number } {
  const isStart = moves[action.move].side === "start";

  if (cell.type === "split") {
    if (isStart) {
      return { part: 2, cursor: 0 };
    } else {
      return { part: 1, cursor: cell.content[0].length };
    }
  } else {
    if (isStart) {
      return { part: 1, cursor: 0 };
    } else {
      return { part: 1, cursor: cell.content?.length ?? 0 };
    }
  }
}
