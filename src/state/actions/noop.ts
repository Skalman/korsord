import type State from "../State";

export interface NoopAction {
  type: "noop";
}

export function noopReducer(state: State, _action: NoopAction): State {
  return state;
}
