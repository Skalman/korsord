import reducer from "../reducer";
import type { Matrix } from "../State";
import type State from "../State";

export interface ReplaceGridAction {
  type: "replaceGrid";
  grid: Matrix;
}

export function replaceGridReducer(
  state: State,
  action: ReplaceGridAction
): State {
  const grid = action.grid;

  const error = getGridError(grid);
  if (error) {
    console.error("Grid invalid:", error);
    return state;
  }

  state = {
    ...state,
    grid,
  };

  state = reducer(state, { type: "historyClear" });

  return state;
}

function getGridError(grid: Matrix) {
  if (!grid) {
    return "Missing grid";
  }

  const rows = grid.rows;
  if (!rows) {
    return "Missing rows";
  }

  // Everything is fine.
}
