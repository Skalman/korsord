import type State from "../State";
import type { Mode } from "../State";

export interface ChangeModeAction {
  type: "changeMode";
  mode: Mode;
}

export function changeModeReducer(
  state: State,
  action: ChangeModeAction
): State {
  if (state.mode === action.mode) {
    return state;
  }

  return {
    ...state,
    mode: action.mode,
  };
}
