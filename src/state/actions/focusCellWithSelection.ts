import type State from "../State";

export interface FocusCellWithSelectionAction {
  type: "focusCellWithSelection";
  cellId: number;
  selection: {
    part: 1 | 2;
    cursor: number;
  };
}

export function focusCellWithSelectionReducer(
  state: State,
  action: FocusCellWithSelectionAction
): State {
  const { focusedCell } = state;
  if (
    focusedCell?.cellId === action.cellId &&
    focusedCell.selection.part === action.selection.part &&
    focusedCell.selection.cursor === action.selection.cursor
  ) {
    return state;
  }

  return {
    ...state,
    focusedCell: {
      cellId: action.cellId,
      selection: action.selection,
    },
  };
}
