import clamp from "../../util/clamp";
import type State from "../State";
import type { History } from "../State";

export interface HistoryGoAction {
  type: "historyGo";
  jump: number;
}

export function historyGoReducer(
  state: State,
  { jump }: HistoryGoAction
): State {
  const newHistory = getNewHistory(state.history, jump);

  if (!newHistory) {
    return state;
  }

  return {
    ...state,
    history: newHistory,
    focusedCell: newHistory.current.focusedCell,
    grid: newHistory.current.grid,
  };
}

function getNewHistory(history: History, jump: number): History | undefined {
  const { past, current, future } = history;

  jump = clamp(-past.length, jump, future.length);

  if (jump < 0) {
    return {
      past: past.slice(0, jump),
      current: past[past.length + jump],
      future: [...past.slice(past.length + jump + 1), current, ...future],
    };
  } else if (jump > 0) {
    return {
      past: [...past, current, ...future.slice(0, Math.max(jump - 2, 0))],
      current: future[jump - 1],
      future: future.slice(jump),
    };
  }
}
