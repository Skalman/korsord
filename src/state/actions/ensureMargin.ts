import clamp from "../../util/clamp";
import findGridCellById from "../../util/findGridCell";
import findLastIndex from "../../util/findLastIndex";
import mapIfDifferent from "../../util/mapIfDifferent";
import type { Matrix, MatrixCell, MatrixCellEmpty, MatrixRow } from "../State";
import type State from "../State";

export interface EnsureMarginAction {
  type: "ensureMargin";
  lastFocusedCellId?: number;
}

export function ensureMarginReducer(
  state: State,
  action: EnsureMarginAction
): State {
  const result = getUpdatedRows(state.grid.rows);

  if (!result) {
    return state;
  }

  const { rows, sizeChange } = result;
  const grid = {
    ...state.grid,
    rows,
  };

  let focusedCell = state.focusedCell;
  if (action.lastFocusedCellId) {
    const { rowIndex, colIndex, cell } = findGridCellById(
      state.grid,
      action.lastFocusedCellId
    );

    const clampedCell = getClampedCell(grid, {
      rowIndex: rowIndex + sizeChange.above,
      colIndex: colIndex + sizeChange.left,
    });

    if (cell !== clampedCell) {
      focusedCell = {
        cellId: clampedCell.cellId,
        selection: { part: 1, cursor: 0 },
      };
    }
  }

  return {
    ...state,
    focusedCell,
    grid,
  };
}

interface SizeChange {
  left: number;
  right: number;
  above: number;
  below: number;
}

function getUpdatedRows(rows: readonly MatrixRow[]) {
  const rowCount = rows.length;
  const colCount = rows[0]?.cells.length ?? 0;

  let add: SizeChange;

  if (rowCount === 0) {
    add = {
      left: 0,
      right: 5,
      above: 0,
      below: 5,
    };
  } else {
    const nonEmpty = firstNonEmptyIndexes(rows);

    if (!nonEmpty) {
      return;
    }

    add = {
      left: 2 - nonEmpty.left,
      right: 2 - (colCount - 1 - nonEmpty.right),
      above: 2 - nonEmpty.top,
      below: 2 - (rowCount - 1 - nonEmpty.bottom),
    };

    if (!add.left && !add.right && !add.above && !add.below) {
      return;
    }
  }

  let maxId = getMaxId(rows);

  const newColCount = colCount + add.left + add.right;
  const newRowCount = rowCount + add.above + add.below;

  if (add.left || add.right) {
    rows = rows.map((row) => ({
      ...row,
      cells: arrayAddBeforeAndAfter(row.cells, add.left, add.right, emptyCell),
    }));
  }

  if (add.above || add.below) {
    rows = arrayAddBeforeAndAfter(rows, add.above, add.below, () =>
      emptyRow(newColCount)
    );
  }

  // Fix `cell.isMargin`.
  rows = mapIfDifferent(rows, (row, rowIndex) => {
    const isRowMargin = rowIndex < 2 || newRowCount - 2 <= rowIndex;

    const cells = mapIfDifferent(row.cells, (cell, colIndex) => {
      const isCellMargin =
        isRowMargin || colIndex < 2 || newColCount - 2 <= colIndex;

      const isMargin = isCellMargin || undefined;

      if (cell.isMargin === isMargin) {
        return cell;
      }

      return { ...cell, isMargin };
    });

    if (cells === row.cells) {
      return row;
    }

    return {
      ...row,
      cells,
    };
  });

  return { rows, sizeChange: add };

  function emptyCell(): MatrixCellEmpty {
    return {
      cellId: ++maxId,
      type: "empty",
      content: undefined,
    };
  }

  function emptyRow(colCount: number): MatrixRow {
    return {
      rowId: ++maxId,
      cells: new Array(colCount).fill(0).map(emptyCell),
    };
  }
}

function firstNonEmptyIndexes(rows: readonly MatrixRow[]) {
  const top = rows.findIndex((row) => !isRowEmpty(row));
  if (top === -1) {
    return;
  }

  return {
    top,
    bottom: findLastIndex(rows, (row) => !isRowEmpty(row)),
    left: rows[0].cells.findIndex((_cell, i) => !isColEmpty(rows, i)),
    right: findLastIndex(rows[0].cells, (_cell, i) => !isColEmpty(rows, i)),
  };
}

function isRowEmpty(row: MatrixRow) {
  return row.cells.every(isCellEmpty);
}

function isColEmpty(rows: readonly MatrixRow[], colIndex: number) {
  return rows.every((row) => isCellEmpty(row.cells[colIndex]));
}

function isCellEmpty(cell: MatrixCell) {
  return cell.type === "empty" && cell.decorations === undefined;
}

function getMaxId(rows: readonly MatrixRow[]) {
  const allIds: number[] = rows
    .map((row) => [row.rowId, row.cells.map((cell) => cell.cellId)])
    .flat(3);

  return Math.max(0, ...allIds);
}

function arrayAddBeforeAndAfter<T>(
  array: readonly T[],
  addBefore: number,
  addAfter: number,
  callback: () => T
) {
  return [
    ...arrayRepeatMap(addBefore <= 0 ? 0 : addBefore, callback),
    ...array.slice(
      addBefore < 0 ? -addBefore : 0,
      addAfter < 0 ? addAfter : undefined
    ),
    ...arrayRepeatMap(addAfter <= 0 ? 0 : addAfter, callback),
  ];
}

function arrayRepeatMap<T>(
  length: number,
  callback: (undefined: undefined, index: number) => T
): T[] {
  return new Array<undefined>(length).fill(undefined).map(callback);
}

function getClampedCell(
  grid: Matrix,
  { rowIndex, colIndex }: { rowIndex: number; colIndex: number }
): MatrixCell {
  const newRowIndex = clamp(0, rowIndex, grid.rows.length - 1);
  const newColIndex = clamp(0, colIndex, grid.rows[0].cells.length - 1);
  return grid.rows[newRowIndex].cells[newColIndex];
}
