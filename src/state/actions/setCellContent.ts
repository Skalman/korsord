import arraysEqual from "../../util/arraysEqual";
import replaceGridCellById from "../../util/replaceGridCell";
import type Action from "../Action";
import reducer from "../reducer";
import type { MatrixCell, MatrixCellSplit } from "../State";
import type State from "../State";

export type SetCellContentAction =
  | {
      type: "setCellContent";
      cellId: number;
      content: string | readonly [string, string];
      splitHintOrientation?: undefined;
    }
  | {
      type: "setCellContent";
      cellId: number;
      content: readonly [string, string];
      splitHintOrientation: "horizontal" | "vertical";
    };

export function setCellContentReducer(
  state: State,
  action: SetCellContentAction
): State {
  const mode = state.mode;

  if (mode === "showWithoutSolution") {
    return state;
  }

  const { cellId } = action;

  let focusAction: Action | undefined;
  const split = splitContent(action);
  if (split) {
    action = split.replacementAction;
    focusAction = split.focusAction;
  }

  const newGrid = replaceGridCellById(state.grid, cellId, (oldCell) => {
    if (mode === "solve") {
      return getUpdatedLetterCell(oldCell, action.content);
    } else if (action.splitHintOrientation === undefined) {
      return getUpdatedCell(oldCell, action.content);
    } else {
      return getUpdatedSplitCell(
        oldCell,
        action.content,
        action.splitHintOrientation
      );
    }
  });

  if (newGrid === state.grid) {
    return state;
  }

  state = {
    ...state,
    grid: newGrid,
  };

  state = reducer(state, {
    type: "ensureMargin",
    lastFocusedCellId: cellId,
  });

  if (focusAction) {
    state = reducer(state, focusAction);
  }

  return state;
}

function getUpdatedLetterCell(
  cell: MatrixCell,
  content: string | readonly [string, string]
): MatrixCell {
  if (
    cell.type === "letter" &&
    typeof content === "string" &&
    content.length <= 1
  ) {
    return {
      ...cell,
      content,
    };
  } else if (
    cell.type === "split" &&
    cell.splitMode === "hintLetter" &&
    Array.isArray(content) &&
    cell.content[0] === content[0] &&
    content[1].length <= 1
  ) {
    return {
      ...cell,
      content,
    };
  } else {
    return cell;
  }
}

function getUpdatedCell(
  cell: MatrixCell,
  content: string | readonly [string, string],
  splitHintOrientation?: "horizontal" | "vertical"
): MatrixCell {
  if (splitHintOrientation !== undefined && Array.isArray(content)) {
    return getUpdatedSplitCell(cell, content, splitHintOrientation);
  }

  if (cell.content === content) {
    return cell;
  }

  if (content === "" || content === " ") {
    return {
      ...cell,
      type: "empty",
      content: undefined,
    };
  } else if (typeof content === "string") {
    return {
      ...cell,
      type: content.length === 1 ? "letter" : "hint",
      content,
    };
  } else {
    return {
      ...cell,
      type: "split",
      splitMode: content[1].length > 1 ? "hint" : "hintLetter",
      content,
    };
  }
}

function getUpdatedSplitCell(
  cell: MatrixCell,
  content: readonly [string, string],
  splitHintOrientation: "horizontal" | "vertical"
): MatrixCellSplit {
  if (
    cell.type === "split" &&
    cell.splitHintOrientation === splitHintOrientation &&
    arraysEqual(cell.content, content)
  ) {
    return cell;
  }

  return {
    ...cell,
    type: "split",
    splitMode: content[1].length > 1 ? "hint" : "hintLetter",
    content,
    splitHintOrientation,
  };
}

function splitContent(
  action: SetCellContentAction
):
  | { replacementAction: SetCellContentAction; focusAction: Action }
  | undefined {
  if (typeof action.content !== "string") {
    return;
  }

  // Split on three or more '-', or on one or more '|'.
  const splitContent = action.content.split(/(-{3,}|\|+)/);
  if (splitContent.length !== 3) {
    return;
  }

  const [part1, split, part2] = splitContent;
  const splitHintOrientation = split[0] === "-" ? "horizontal" : "vertical";

  return {
    focusAction: {
      type: "focusCellWithSelection",
      cellId: action.cellId,
      selection: {
        part: part1 === "" && part2 !== "" ? 1 : 2,
        cursor: 0,
      },
    },

    replacementAction: {
      ...action,
      content: [part1, part2],
      splitHintOrientation,
    },
  };
}
