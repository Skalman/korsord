import arraysEqual from "../../util/arraysEqual";
import replaceGridCellById from "../../util/replaceGridCell";
import type { CellDecorations, MatrixCell } from "../State";
import type State from "../State";
import { ensureMarginReducer } from "./ensureMargin";

export interface SetCellDecorationsAction {
  type: "setCellDecorations";
  cellId: number;
  decorations?: readonly CellDecorations[];
}

export function setCellDecorationsReducer(
  state: State,
  { cellId, decorations }: SetCellDecorationsAction
): State {
  if (decorations?.length === 0) {
    decorations = undefined;
  }

  const newGrid = replaceGridCellById(state.grid, cellId, (oldCell) =>
    getUpdatedCell(oldCell, decorations)
  );

  if (newGrid === state.grid) {
    return state;
  }

  state = {
    ...state,
    grid: newGrid,
  };

  return ensureMarginReducer(state, {
    type: "ensureMargin",
    lastFocusedCellId: cellId,
  });
}

function getUpdatedCell(
  cell: MatrixCell,
  decorations?: readonly CellDecorations[]
) {
  if (
    cell.decorations === decorations ||
    arraysEqual(cell.decorations, decorations)
  ) {
    return cell;
  }

  return {
    ...cell,
    decorations,
  };
}
