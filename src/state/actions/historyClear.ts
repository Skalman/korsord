import type State from "../State";

export interface HistoryClearAction {
  type: "historyClear";
}

export function historyClearReducer(
  state: State,
  _action: HistoryClearAction
): State {
  return {
    ...state,
    history: {
      past: [],
      current: {
        grid: state.grid,
        focusedCell: state.focusedCell,
      },
      future: [],
    },
  };
}
