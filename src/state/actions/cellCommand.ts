import { getToggleBlack } from "../../commands/black";
import { getToggleSplit } from "../../commands/split";
import { getToggleSymbol } from "../../commands/symbol";
import { getToggleVerticalSplit } from "../../commands/verticalSplit";
import type { SymbolKey } from "../../components/SvgSymbol";
import findGridCellById from "../../util/findGridCell";
import reducer from "../reducer";
import type State from "../State";

export type CellCommandAction =
  | {
      type: "cellCommand";
      command: "toggleBlack" | "toggleSplit" | "toggleVerticalSplit";
    }
  | {
      type: "cellCommand";
      command: "toggleSymbol";
      symbolKey: SymbolKey;
    };

export function cellCommandReducer(
  state: State,
  action: CellCommandAction
): State {
  const commandAction = getCommandAction(state, action) ?? { type: "noop" };
  return reducer(state, commandAction);
}

function getCommandAction(state: State, action: CellCommandAction) {
  const focusedCellId = state.focusedCell?.cellId;

  if (!focusedCellId) {
    return;
  }

  const { cell } = findGridCellById(state.grid, focusedCellId);
  if (!cell) {
    return;
  }

  switch (action.command) {
    case "toggleBlack":
      return getToggleBlack(cell);

    case "toggleSplit":
      return getToggleSplit(cell);

    case "toggleVerticalSplit":
      return getToggleVerticalSplit(cell);

    case "toggleSymbol":
      return getToggleSymbol(cell, action.symbolKey);
  }
}
