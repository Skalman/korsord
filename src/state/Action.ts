import type { CellCommandAction } from "./actions/cellCommand";
import type { ChangeModeAction } from "./actions/changeMode";
import type { EnsureMarginAction } from "./actions/ensureMargin";
import type { FocusCellBlurAction } from "./actions/focusCellBlur";
import type { FocusCellMoveAction } from "./actions/focusCellMove";
import type { FocusCellWithSelectionAction } from "./actions/focusCellWithSelection";
import type { HistoryClearAction } from "./actions/historyClear";
import type { HistoryGoAction } from "./actions/historyGo";
import type { HistoryUpdateAction } from "./actions/historyUpdate";
import type { NoopAction } from "./actions/noop";
import type { ReplaceGridAction } from "./actions/replaceGrid";
import type { SetCellContentAction } from "./actions/setCellContent";
import type { SetCellDecorationsAction } from "./actions/setCellDecorations";

type Action =
  | CellCommandAction
  | ChangeModeAction
  | EnsureMarginAction
  | FocusCellBlurAction
  | FocusCellMoveAction
  | FocusCellWithSelectionAction
  | HistoryClearAction
  | HistoryGoAction
  | HistoryUpdateAction
  | NoopAction
  | ReplaceGridAction
  | SetCellContentAction
  | SetCellDecorationsAction;

export default Action;
