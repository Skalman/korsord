import React from "react";

import noop from "../util/noop";
import type Action from "./Action";
import initialState from "./initialState";
import type State from "./State";

interface StateContext {
  state: State;
  dispatch(action: Action): void;
}

const StateContext = React.createContext<StateContext>({
  state: initialState,
  dispatch: noop,
});

export default StateContext;
