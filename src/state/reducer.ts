import type Action from "./Action";
import { cellCommandReducer } from "./actions/cellCommand";
import { changeModeReducer } from "./actions/changeMode";
import { ensureMarginReducer } from "./actions/ensureMargin";
import { focusCellBlurReducer } from "./actions/focusCellBlur";
import { focusCellMoveReducer } from "./actions/focusCellMove";
import { focusCellWithSelectionReducer } from "./actions/focusCellWithSelection";
import { historyClearReducer } from "./actions/historyClear";
import { historyGoReducer } from "./actions/historyGo";
import { historyUpdateReducer } from "./actions/historyUpdate";
import { noopReducer } from "./actions/noop";
import { replaceGridReducer } from "./actions/replaceGrid";
import { setCellContentReducer } from "./actions/setCellContent";
import { setCellDecorationsReducer } from "./actions/setCellDecorations";
import type State from "./State";

const reducerLookup: ReducerLookup = {
  cellCommand: cellCommandReducer,
  changeMode: changeModeReducer,
  ensureMargin: ensureMarginReducer,
  focusCellBlur: focusCellBlurReducer,
  focusCellMove: focusCellMoveReducer,
  focusCellWithSelection: focusCellWithSelectionReducer,
  historyClear: historyClearReducer,
  historyGo: historyGoReducer,
  historyUpdate: historyUpdateReducer,
  noop: noopReducer,
  replaceGrid: replaceGridReducer,
  setCellContent: setCellContentReducer,
  setCellDecorations: setCellDecorationsReducer,
};

function reducer(state: State, action: Action): State {
  // Typescript can't figure out that `action` definitely will have the correct
  // type.
  const matchedReducer = reducerLookup[action.type] as Reducer<Action>;

  if (!matchedReducer) {
    console.error("Got invalid action", action);
    throw new Error("Got invalid action");
  }

  return matchedReducer(state, action);
}

export function historyEnhancedReducer(state: State, action: Action): State {
  let newState = reducer(state, action);
  newState = reducer(newState, {
    type: "historyUpdate",
    prevState: state,
  });
  return newState;
}

export default reducer;

type GetActionType<
  TType extends Action["type"],
  TAction extends Action = Action
> = TAction extends {
  type: TType;
}
  ? TAction
  : never;

type Reducer<T extends Action> = (state: State, action: T) => State;

type ReducerLookup = {
  [TType in Action["type"]]: Reducer<GetActionType<TType>>;
};
