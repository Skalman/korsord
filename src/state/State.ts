import type { SymbolKey } from "../components/SvgSymbol";

interface State {
  readonly grid: Matrix;
  readonly focusedCell?: FocusedCell;
  readonly history: History;
  readonly mode: Mode;
}

export type Mode = "edit" | "showWithoutSolution" | "solve";

export interface History {
  readonly past: readonly HistoryItem[];
  readonly current: HistoryItem;
  readonly future: readonly HistoryItem[];
}

export interface HistoryItem {
  readonly grid: Matrix;
  readonly focusedCell?: FocusedCell;
}

export interface FocusedCell {
  readonly cellId: number;
  readonly selection: {
    readonly part: 1 | 2;
    readonly cursor: number;
  };
}

export type CellDecorations =
  | { type: "black" }
  | { type: "symbol"; key: SymbolKey };

export interface Matrix {
  readonly type: "korsord";
  readonly version: 1;
  readonly forkedFrom?: string;
  readonly rows: readonly MatrixRow[];
}

export interface MatrixRow {
  readonly rowId: number;
  readonly cells: readonly MatrixCell[];
}

interface MatrixCellBase {
  readonly cellId: number;
  readonly type: string;
  readonly content?: string | readonly [string, string];
  readonly decorations?: readonly CellDecorations[];

  /** Only relevant to split cells. */
  readonly splitMode?: "hint" | "hintLetter";
  /** Only relevant to split cells. */
  readonly splitHintOrientation?: "horizontal" | "vertical";
  /** Only relevant to empty cells. */
  readonly isMargin?: true;
}

export interface MatrixCellLetter extends MatrixCellBase {
  readonly type: "letter";
  readonly content: string;
}

export interface MatrixCellHint extends MatrixCellBase {
  readonly type: "hint";
  readonly content: string;
}

export interface MatrixCellSplit extends MatrixCellBase {
  readonly type: "split";
  readonly content: readonly [string, string];
  // Not optional.
  readonly splitMode: "hint" | "hintLetter";
  // Copied here for clarity.
  readonly splitHintOrientation?: "horizontal" | "vertical";
}

export interface MatrixCellEmpty extends MatrixCellBase {
  readonly type: "empty";
  readonly content: undefined;
  // Copied here for clarity.
  readonly isMargin?: true;
}

export type MatrixCell =
  | MatrixCellLetter
  | MatrixCellHint
  | MatrixCellSplit
  | MatrixCellEmpty;

export default State;
