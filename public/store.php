<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$env = getDotEnv();

cors($env);

if (!class_exists('Mysqli')) {
    output(500, [
        'message' => 'Mysqli is not supported',
    ]);
    exit();
}

$mysqli = new Mysqli(
    $env['API_MYSQL_HOST'],
    $env['API_MYSQL_USER'],
    $env['API_MYSQL_PASSWORD'],
    $env['API_MYSQL_DATABASE']
);

initDatabase($mysqli);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        getItem($mysqli, @$_GET['id']);
        break;

    case 'POST':
        postItem($mysqli, getJsonBody());
        break;

    case 'PUT':
        putItem($mysqli, @$_GET['id'], getJsonBody());
        break;
}

function output($status, $object)
{
    global $env;

    header("Status: $status");
    header('Content-Type: application/json');
    header('Cache-Control: no-store');

    if (is_string($object)) {
        echo $object;
    } else {
        echo json_encode($object);
    }
}

function cors($env)
{
    if (!isset($_SERVER['HTTP_ORIGIN'])) {
        return;
    }

    if ($env['API_CORS_ALLOW'] !== $_SERVER['HTTP_ORIGIN']) {
        output(400, [
            'message' => "Request from $_SERVER[HTTP_ORIGIN] not allowed",
        ]);
        exit();
    }

    header("Access-Control-Allow-Origin: $env[API_CORS_ALLOW]");
    header('Access-Control-Max-Age: 86400');
    // Access-Control-Allow-Credentials not needed.

    if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS');
        header('Access-Control-Allow-Headers: *');
        exit();
    }
}

function getDotEnv()
{
    $dir = __DIR__;

    while (true) {
        if (file_exists("$dir/.env")) {
            $content = file("$dir/.env");
            $content = preg_grep('/\s*[a-z0-9]+\s*=/i', $content);
            $content = array_map('trim', $content);
            $content = array_map(function ($line) {
                [$key, $value] = preg_split('/\s*=\s*/', $line, 2);
                return [$key => $value];
            }, $content);
            $content = array_merge(...$content);

            return $content;
        }

        if ($dir === '/') {
            return [];
        }

        $dir = dirname($dir);
    }
}

function initDatabase($mysqli)
{
    $dbExists = $mysqli->query(
        'CREATE TABLE IF NOT EXISTS store (
            id varchar(40) NOT NULL,
            password varchar(40) NOT NULL,
            version int NOT NULL,
            value text NOT NULL,
            created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8'
    );
}

function getJsonBody()
{
    $body = file_get_contents('php://input');
    $json = json_decode($body, true);
    if ($json === null) {
        output(400, [
            'message' => 'Invalid JSON',
        ]);
        exit();
    }
    return $json;
}

function query($mysqli, $query, ...$params)
{
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        output(500, 'Query error: ' . $mysqli->error);
        exit();
    }

    $stmt->bind_param(str_repeat('s', count($params)), ...$params);
    $stmt->execute();
    return $stmt;
}

function generateRandom()
{
    return str_replace(['+', '/', '='], '', base64_encode(random_bytes(8)));
}

function getRow($mysqli, $id)
{
    $stmt = query($mysqli, 'SELECT * FROM store WHERE id = ? LIMIT 1', $id);

    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    return $row;
}

function getItem($mysqli, $id)
{
    $row = getRow($mysqli, $id);

    if ($row === null) {
        output(404, [
            'message' => "No item with ID '$id' found",
        ]);
    } else {
        $value = json_decode($row['value']);

        if ($value === null) {
            output(500, [
                'message' => "Internal error: Invalid data format for '$id'. Contact the site owner",
            ]);
        } else {
            output(200, [
                'value' => $value,
                'version' => $row['version'],
            ]);
        }
    }
}

function postItem($mysqli, $body)
{
    $id = generateRandom();
    $password = generateRandom();
    $version = 1;
    $value = @$body['value'];

    if ($value === null) {
        output(400, [
            'message' => 'Missing value',
        ]);
    }

    $stmt = query(
        $mysqli,
        'INSERT INTO store (id, password, version, value)
        VALUES (?, ?, ?, ?)',
        $id,
        $password,
        $version,
        json_encode($value)
    );

    if ($stmt->affected_rows !== 1) {
        output(500, [
            'message' => 'Failed to create item',
        ]);
    } else {
        output(201, [
            'id' => $id,
            'password' => $password,
            'version' => $version,
            'url' => "$_SERVER[SCRIPT_NAME]?id=$id",
        ]);
    }
}

function putItem($mysqli, $id, $body)
{
    $mysqli->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

    $row = getRow($mysqli, $id);

    if ($row === null) {
        output(404, [
            'message' => "No item with ID '$id' found",
        ]);
    } elseif ($row['password'] !== $body['password']) {
        output(401, [
            'message' => 'Password is incorrect',
        ]);
    } elseif ($row['version'] !== $body['version']) {
        output(409, [
            'message' => 'Edit confict; you need to fetch the updated version',
        ]);
    } elseif ($row['value'] === $body['value']) {
        output(200, [
            'message' => 'No update needed',
        ]);
    } else {
        $stmt = query(
            $mysqli,
            'UPDATE store
            SET value = ?, version = ?
            WHERE id = ? AND password = ? AND version = ?',
            json_encode($body['value']),
            $body['version'] + 1,
            $id,
            $body['password'],
            $body['version']
        );

        if ($stmt->affected_rows === 1) {
            output(200, [
                'version' => $body['version'] + 1,
                'message' => 'Item updated',
            ]);
        } else {
            $mysqli->rollback();
            output(500, [
                'message' => 'Unexpected server error',
            ]);
            return;
        }
    }

    $mysqli->commit();
}
